const startRestartButtons = document.querySelectorAll(".startrestart");

const grayBg = document.querySelector(".graybg");
const patternChoice = document.querySelector(".patternchoice");

const patternX = document.getElementById("X");
const patternO = document.getElementById("O");

const endDiv = document.querySelector(".end");
const textEnd = document.getElementById("endtext");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const gameBoard = (() => {
  let board = [0, 0, 0, 0, 0, 0, 0, 0, 0];

  const putMark = (index, player) => {
    if (board[index] == 0) {
      board[index] = player.getPattern();
      return true;
    }
    return false;
  };

  const restart = () => (board = [0, 0, 0, 0, 0, 0, 0, 0, 0]);

  const getBoard = () => board;

  const getEmptyFieldsIdx = () => {
    fields = [];
    for (let i = 0; i < board.length; i++) {
      const field = board[i];
      if (field == 0) {
        fields.push(i);
      }
    }
    return fields;
  };

  return { getBoard, putMark, restart, getEmptyFieldsIdx };
})();

const player = (pattern) => {
  const setPattern = () => {};

  const getPattern = () => pattern;

  return { getPattern, setPattern };
};

const gameController = (() => {
  let humanPlayer;
  let aiPlayer;
  let board;
  const boxes = document.querySelectorAll(".box");
  let currentPlayer;
  let aiPattern;


  const getAiPlayer = () => {
    return aiPlayer;
  }

  const getHumanPlayer = () => {
    return humanPlayer;
  }

  const startRestart = (i) => {
    board = gameBoard.getBoard();
    humanPlayer = player(i);
    aiPlayer = player(-i + 3);
    aiPattern = -i+3;
    if (i == 1) {
      currentPlayer = humanPlayer;
    } else {
      currentPlayer = aiPlayer;
    }
  };

  const checkEnd = () => {
    for (let i = 0; i < 3; i++) {
      if (
        board[i] != 0 &&
        board[i] === board[i + 3] &&
        board[i] === board[i + 6]
      ) {
        return board[i];
      }
    }
    for (let i = 0; i < 7; i += 3) {
      if (
        board[i] != 0 &&
        board[i] === board[i + 1] &&
        board[i] === board[i + 2]
      ) {
        return board[i];
      }
    }
    if (board[0] != 0 && board[0] == board[4] && board[0] === board[8]) {
      return board[0];
    }
    if (board[2] != 0 && board[2] == board[4] && board[2] === board[6]) {
      return board[2];
    }

    if (!board.includes(0)) {
      return 0;
    }

    return -1;
  };

  const playerMove = (index) => {
    const board = gameBoard.getBoard();
    if (board[index] === 0) {
      const boolChanged = gameBoard.putMark(index, currentPlayer);
      if (boolChanged) {
        const box = document.getElementById(`${index}`);
        box.classList.remove("noMark");
        const check = checkEnd();
        if (check > -1) {
          endGame(check);
        } else {
          switchPlayer();
          if (currentPlayer == aiPlayer){
            const aiChoice = aiControl.chooseField();
            playerMove(aiChoice);
          } 
        }
      }
    }
  };

  const switchPlayer = () => {
    currentPlayer === humanPlayer
      ? (currentPlayer = aiPlayer)
      : (currentPlayer = humanPlayer);
    boxes.forEach((box) => {
      box.removeEventListener("click", handlePlayerMove);
    });
    boxes.forEach((box) => {
      box.addEventListener("click", handlePlayerMove);
      if (box.classList.contains("noMark")) {
        const pattern = currentPlayer.getPattern();
        if (pattern === 1) {
          box.textContent = "X";
        } else {
          box.textContent = "O";
        }
      }
    });
  };

  const endGame = (resCheck) => {
    boxes.forEach((box) => {
      box.removeEventListener("click", handlePlayerMove);
      if (box.classList.contains("noMark")) {
        box.classList.remove("noMark");
        box.textContent = "";
      }
    });
    if (resCheck == 0) {
      displayEnd("Draw");
    } else if (resCheck == 1) {
      displayEnd("X's wins");
    } else {
      displayEnd("O's wins");
    }
  };

  const displayEnd = (string) => {
    textEnd.textContent = string;
    endDiv.classList.add("active");
    grayBg.classList.add("active");
  }

  const handlePlayerMove = (event) => {
    const index = event.target.getAttribute("id");
    playerMove(index);
  };

  return {
    startRestart,
    handlePlayerMove,
    checkEnd,
    currentPlayer,
    aiPattern,
    getAiPlayer,
    getHumanPlayer,
    playerMove
  };
})();




const displayController = ((gameController) => {
  const boxes = document.querySelectorAll(".box");

  const startRestart = () => {
    boxes.forEach((box) => {
      box.classList.add("noMark");
      box.textContent = "X";
    });

    boxes.forEach((box) => {
      box.addEventListener("click", gameController.handlePlayerMove);
    });
  };

  return { startRestart };
})(gameController);



const aiControl = (() => {

  const chooseField = () => {
    let choice = null;
    choice = minimax(gameBoard, gameController.getAiPlayer()).index;
    const field = gameBoard.getBoard()[choice];
    if (field != 0) {
      return "error";
    }
    return choice;
  };



  const findBestMove = (moves, player) => {
    let bestMove;
    if (player === gameController.getAiPlayer()) {
      let bestScore = -10000;
      for (let i = 0; i < moves.length; i++) {
        if (moves[i].score > bestScore) {
          bestScore = moves[i].score;
          bestMove = i;
        }
      }
    } else {
      let bestScore = 10000;
      for (let i = 0; i < moves.length; i++) {
        if (moves[i].score < bestScore) {
          bestScore = moves[i].score;
          bestMove = i;
        }
      }
    }
    return moves[bestMove];
  };

  /**
   * Returns an object which includes the 'index' and the 'score' of the next best move
   * @param {gameBoard} newBoard - call it with the gameBoard
   * @param {player} player - call it with the AI player
   */
  const minimax = (newBoard, player) => {
    let empty = newBoard.getEmptyFieldsIdx();
    let check = gameController.checkEnd();

    if (check == 0) {
      return {
        score: 0,
      };
    } else if (check>0) {
      if (player.getPattern() == gameController.getHumanPlayer().getPattern()) {
        return {
          score: 10,
        };
      } else if (player.getPattern() == gameController.getAiPlayer().getPattern()) {
        return {
          score: -10,
        };
      }
    }


    let moves = [];

    for (let i = 0; i < empty.length; i++) {
      let move = {};
      move.index = empty[i];

      //Change the field value to the sign of the player
      newBoard.getBoard()[empty[i]] = player.getPattern();

      //Call the minimax with the opposite player
      if (player.getPattern() == gameController.getAiPlayer().getPattern()) {
        let result = minimax(newBoard, gameController.getHumanPlayer());
        move.score = result.score;
      } else {
        let result = minimax(newBoard, gameController.getAiPlayer());
        move.score = result.score;
      }

      //Reset the filed value set before
      newBoard.getBoard()[empty[i]] = 0;
       
      moves.push(move);
    }

    //find the best move
    const res = findBestMove(moves,player);
    return res;
  };

  return {chooseField};
})();


grayBg.addEventListener("click", () => {
  grayBg.classList.remove("active");
  patternChoice.classList.remove("active");
});

startRestartButtons.forEach((button) => {
  button.addEventListener("click", () => {
    grayBg.classList.add("active");
    patternChoice.classList.add("active");
    button.textContent = "Restart";
  });
});

const endRestart = document.getElementById("endrestart");

endRestart.addEventListener("click",() => {
  endDiv.classList.remove("active");
})

patternX.addEventListener("click", () => {
  initializeGame(1);
});

patternO.addEventListener("click", () => {
  initializeGame(2);
  const aiChoice = aiControl.chooseField();
  gameController.playerMove(aiChoice);

});

function initializeGame(i) {
  displayController.startRestart();
  gameBoard.restart();
  gameController.startRestart(i);
  grayBg.classList.remove("active");
  patternChoice.classList.remove("active");
}
